const path    = require('path'),
      webpack = require('webpack'),
      pkg     = require('./package.json');

const config = {
  entry: {
    app: [
      'babel-es6-polyfill',
      './js/index.js'
    ]
  },

  output: {
    path    : './dist',
    filename: `${pkg.name}.js`
  },

  module: {
    rules: [{
      enforce: 'pre',
      test   : /\.js$/,
      include: [
        path.resolve(__dirname, 'js')
      ],
      use    : [{
        loader : 'babel-loader',
        options: {
          presets: [['es2015', {modules: false}]]
        }
      }],
    }],
  },

  plugins: []
};

module.exports = function (env) {
  switch (env.build) {
    case 'development':
      config.devtool = '#inline-source-map';
      break;

    case 'production':
    default:
      config.plugins.push(new webpack.optimize.UglifyJsPlugin());
      break;
  }

  return config;
};
